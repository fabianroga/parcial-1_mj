package com.sigmotoa.parcial1

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class   MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnNext: Button = findViewById(R.id.btnP)

        btnNext.setOnClickListener { lookValue() }




    }

    fun lookValue(){
        val etName = findViewById<EditText>(R.id.editTextTextPersonName)
        if (etName.text.isNotEmpty()){
            val intent = Intent (this, Segunda::class.java)
            intent.putExtra("INTENT_NAME",etName.text)
            startActivity(intent)
        }
        else{
            showError()
        }

    }
    fun showError(){
        Toast.makeText(this,"ErrorText",Toast.LENGTH_SHORT).show()
    }
}